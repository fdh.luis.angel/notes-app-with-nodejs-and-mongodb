const router = require('express').Router();
const User = require('../models/User');
const passport = require('passport');

router.get('/users/singin', (req, res) =>{
    res.render('users/singin');
});

router.post('/users/singin', passport.authenticate('local', {
    successRedirect:'/notes',
    failureRedirect:'/users/singin',
    failureFlash:true
}));

router.get('/users/singup', (req, res) =>{
    res.render('users/singup');
});

router.post('/users/singup', async (req, res) =>{
     
    //console.log(req.body);
    const {name, email, password, confirm_password} = req.body;
    const errors = [];

    if(name.length == 0 || password.length == 0 || email.length == 0){
        errors.push({text:'Error fields empty'});
    }
    else{
        if(password != confirm_password){
            errors.push({text:'Password do not match'});
        }
        if(password.length < 4){
            errors.push({text:'Password must be at least 4 cacacters'});
        }
    }

    

    if(errors.length > 0){
        res.render('users/singup',{errors, name, email, password, confirm_password});
    }
    else{
        const emailUser = await User.findOne({email:email});
        
        if(emailUser){
            //errors.push({text:'Email already registered, try with other'});
            
            //res.render('users/singup',{errors, name, email, password, confirm_password});
            console.log("Correo en uso");
            req.flash('error_msg', 'Email is already in use');
            res.redirect('/users/singup');
            return;
        }
        else{
            const newUser = new User({name, email, password});
            newUser.password = await newUser.encyptPassword(password);
            await newUser.save();
            req.flash('success_msg', 'You are registered');
            res.redirect('/users/singin');
        }
    }
});

router.get('/users/logout', (req, res) =>{
    req.logOut();
    res.redirect('/');
});



module.exports = router;