const router = require('express').Router();
const Note = require('../models/Note');
const{isAuthenticated} = require('../helpers/auth');


router.get('/notes/add', isAuthenticated,(req, res) =>{
    res.render('notes/new-note');
});

router.post('/notes/new-note', isAuthenticated,async (req, res) =>{
    //console.log(req.body);
    const info = {
        title: req.body.title, 
        description: req.body.description, 
        user: req.user.id, 
    };

    const errors = [];

    if(!info.title){
        errors.push({text:"Por favor ingrese un titulo"});
    }
    if(!info.description){
        errors.push({text:"Por favor ingrese una descripcion"});
    }

    if(errors.length>0){
        res.render('notes/new-note', {
            errors,
            title, 
            description
        });
    }
    else {
        const {title, description, user} = info;
        const newNote = new Note({title, description, user});
        console.log("New note info");
        console.log(newNote);
        await newNote.save();
        req.flash('success_msg', 'Note added successfully');
        res.redirect('/notes');
    }
});

router.get('/notes', isAuthenticated,async (req, res) =>{
    //console.log(req.user.name)
    //const notes = await Note.find();

    const username = req.user.name; 

    await Note.find({user:req.user.id}).sort({date:"desc"})
        .then(documentos => {
            const contexto = {
                notes: documentos.map(documento => {
                    return {
                        id:documento.id,
                        title: documento.title,
                        description: documento.description,
                        user:documento.user
                    }
                })
            }

            if(contexto.notes.length==0){
                res.render('notes/all-notes', {username}); 
            }
            else{
                res.render('notes/all-notes', {notes: contexto.notes}); 
            }
        });
});


router.get('/notes/edit/:id', isAuthenticated,async (req, res) =>{
    const note = await Note.findById(req.params.id);
    const {id,title,description} = note;
    res.render('notes/edit-note', { id, title, description });
});

router.put('/notes/edit-note/:id', isAuthenticated,async (req, res) =>{
    const {title, description} = req.body;
    console.log(req.body);
    await Note.findByIdAndUpdate(req.params.id, {title, description});
    req.flash('success_msg', 'Note updated successfully');
    res.redirect('/notes');
});

router.delete('/notes/delete/:id', isAuthenticated,async (req, res) =>{
    await Note.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'Note deleted successfully');
    res.redirect("/notes");
});


module.exports = router;